package com.example.chandan.smartbeingstask;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.job.JobParameters;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import static com.example.chandan.smartbeingstask.MainActivity.msg;

/**
 * Created by chandan on 27/11/17.
 */

public class TestJobServiceOne extends Service {
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(this, "Job Started", Toast.LENGTH_SHORT).show();
        Log.e("Done","at Test Job");
        sendNotification();
        return super.onStartCommand(intent, flags, startId);
    }

    public void sendNotification() {

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this);

//Create the intent that’ll fire when the user taps the notification//

        Intent intent = new Intent();
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 1, intent, 0);

        mBuilder.setContentIntent(pendingIntent);
        mBuilder.setSmallIcon(R.mipmap.ic_launcher);
        mBuilder.setContentTitle("My Message");
        mBuilder.setContentText(msg);

        NotificationManager mNotificationManager =

                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.notify(1, mBuilder.build());
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "Job Destroyed", Toast.LENGTH_SHORT).show();
        Log.e("Done","Out from test");
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
