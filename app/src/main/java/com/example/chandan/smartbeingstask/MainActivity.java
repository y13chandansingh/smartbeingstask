package com.example.chandan.smartbeingstask;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    private EditText message;
    private EditText time;
    private TextView btnSchedule;
    public static String msg;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = LayoutInflater.from(this).inflate(
                R.layout.activity_main, null, true);
        setContentView(view);
        initView();
    }

    private void initView() {
        message = (EditText) findViewById(R.id.edtEnterMessage);
        time = (EditText) findViewById(R.id.edtEnterTime);
        btnSchedule = (TextView) findViewById(R.id.txtSchedule);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        btnSchedule.setOnClickListener(this);
    }


    private void startJob(int time) {

        //      alarm.cancel(pintent);
       // if the android version is greater than lollipop
        //job schedular comes from marshallow
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ComponentName serviceComponent = new ComponentName(this, TestJobService.class);
            JobInfo.Builder builder = new JobInfo.Builder(0, serviceComponent);
//            builder.setMinimumLatency(1000); // wait at least
//            builder.setOverrideDeadline(50 * 1000); // maximum delay
            builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED); // require unmetered network
            builder.setRequiresDeviceIdle(true); // device should be idle
            builder.setRequiresCharging(false); // we don't care if the device is charging or not
            builder.setPeriodic(time);
            JobScheduler jobScheduler = this.getSystemService(JobScheduler.class);
            jobScheduler.schedule(builder.build());
        } else {
            Intent intent = new Intent(this, TestJobServiceOne.class);
            PendingIntent pintent = PendingIntent.getService(this, 0, intent, 0);
            AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            alarm.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), time, pintent);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtSchedule:
                sharedPreferences.edit().putString("message",message.getText().toString()).commit();
                sharedPreferences.edit().putInt("time", Integer.parseInt(time.getText().toString())).commit();
                Log.d("Valueatshared",sharedPreferences.getString("message","notfound"));
                msg = sharedPreferences.getString("message","not found");
                int time = sharedPreferences.getInt("time", 1);
                time = time *1000;
                startJob(time);
                startService(new Intent(MainActivity.this, TestJobService.class));
                break;
        }
    }
}
